/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function helloMessage(){
			let naMe = prompt("What is your name? ");
			let ageYrs = prompt("How old are you? ");
			let location = prompt("Where do you live? ");

			console.log(`Hello, ${naMe} `)
			console.log(`Your age is ${ageYrs}`)
			console.log(`You live in ${location}`)
			
	}

		helloMessage();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function displayFavBand(){
			console.log("1. When in Rome ");
			console.log("2. Oasis");
			console.log("3. Arctic Monkey");
			console.log("4. Red hot chilli pepper");
			console.log("5. Coldplay");
	}

		displayFavBand();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function displayFavMov(){
			console.log("1. The Shawshank redemption");
			console.log("Rotten tomatoes rating: 91%")
			console.log("2. The Godfather");
			console.log("Rotten tomatoes rating: 97%")
			console.log("3. The Dark Knight");
			console.log("Rotten tomatoes rating: 94%")
			console.log("4. Schindler's List");
			console.log("Rotten tomatoes rating: 98%")
			console.log("5. Pulp Fiction");
			console.log("Rotten tomatoes rating: 92%")
	}

		displayFavMov();


	

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


 	function printUsers(){
			alert("Hi! Please add the names of your friends.");
			let friend1 = prompt("Enter your first friend's name:"); 
			let friend2 = prompt("Enter your second friend's name:"); 
			let friend3 = prompt("Enter your third friend's name:");

			console.log("You are friends with:")
			console.log(friend1); 
			console.log(friend2); 
			console.log(friend3); 
	};


	printUsers();